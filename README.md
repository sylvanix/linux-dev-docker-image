# **A Linux Docker Image for Development**
*Build and run a Docker development container for either GCC (C & C++), Python, Elixir, or Haskell Development*  

Custom packages, as defined in the Dockerfiles, will be installed. The Python modules listed in requirements.txt are installed using pip.

Build and then run the development container with the scirpt ```build_run.sh```.
The script ```enter_container.sh``` script is used to enter the running container from a different terminal window on the host.
