#!/bin/bash

username="syl"
imgname="syldev"
tag="tf2py"
containername="syl-tf2py"

#setfacl -m user:1000:r /home/${username}/.Xauthority

# run the image and drop into the container's shell
docker run \
    -it \
    --rm \
    --name ${containername} \
    --net=host \
    -h tf2pynix \
    -e DISPLAY \
    -v ${HOME}/.Xauthority:/home/${username}/.Xauthority \
    -v ${HOME}/1210SRD:/1210SRD \
    ${imgname}:${tag} \
    env TERM=xterm-256color \
    /bin/bash
