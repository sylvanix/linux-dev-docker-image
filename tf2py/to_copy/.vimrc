
" use colors as defined in ~/.vim/colors/
colorscheme antares

" don't worry about compatibility with vi
set nocompatible

" enable syntax and plugins for netrw (a built-in plugin for Vim)
"syntax on
syntax enable
filetype plugin on

" display all matching files with tab complete when using :find
set wildmenu

highlight Normal guibg=black guifg=white
set background=dark
set termguicolors

" font & font size
set guifont=hack\ 11
"set guifont=monospace\ 11

" default window size
"set lines=49
"set columns=107

" right column color & position
set colorcolumn=99
"highlight ColorColumn ctermbg=0 guibg=grey20
set textwidth=0
set wrap

" Insert space characters whenever the tab key is pressed (not tabs).
" - to insert real tabs, like in a Makefile, use the Ctrl-V <Tab> sequence.
set tabstop=4 shiftwidth=4 expandtab softtabstop=4

" show the search count
set shortmess-=S

" misc
set number
set ignorecase
set ts=4
"set spell spelllang=en_uk

" remap esc in insert mode
inoremap kj <esc>

" remap esc in command mode
cnoremap kj <C-C>
" Note: In command mode mappings to esc run the command for some odd
" historical vi compatibility reason. We use the alternate method of
" existing which is Ctrl-C

"remap a sequence for commonly used printf("",);
inoremap ppp printf("",);

" display the status line
set laststatus=2

" format the tab label
" %N ---> tab number
" %t ---> file name
" %M ---> shows a + sign if modified
set guitablabel=\[%N\]\%t\ %M

set hlsearch
" Press Space to turn off highlighting and clear any message already displayed
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" no fucking error bell!
set belloff=all

highlight SpecialKey ctermfg=1
set list
set listchars=tab:T>

" disable automatic comment insertion which gets triggered from plugins
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

set t_Co=256

"Splitting
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

"NERDTREE
" Shortcut to open NERDTree
map <C-n> :NERDTreeToggle<CR>
" add ability to close Vim if only open window is NERDTree
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

