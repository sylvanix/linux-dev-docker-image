#!/bin/bash

username="syl"
imgname="syldev"
tag="tf2py"
containername="dev-tf2py"

# build from the Dockerfile
docker build . --tag ${imgname}:${tag}
