 # pull the best Linux image
FROM haskell:9.4.4-buster

ARG USERNAME=syl
ARG USERID=1000
ARG GNAME="syl"
ARG GID=1000

# always combine "update" with "install" in a RUN statement
RUN apt-get update && apt-get install -y --no-install-recommends \
                                      curl \
                                      git \
                                      nemo \
                                      less \
                                      vim \
                                      ranger \
                                      neofetch \
                                      htop \
                                      pluma \
                                      eog \
                                      ffmpeg \
                                      tmux \
                                      fonts-hack-ttf \
                                      lolcat \
                                      cowsay \                           
                                      fortune \
                                      fortunes \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g $GID $GNAME \
    && useradd -ms /bin/bash --uid $USERID --gid $GID \
               --groups $GNAME $USERNAME --password nixpwd

RUN cabal update

WORKDIR /home/$USERNAME
USER $USERNAME

RUN mkdir -p /home/$USERNAME/.config/ranger
ADD ./to_copy/.ghci/ /home/$USERNAME
ADD ./to_copy/ranger/ /home/$USERNAME/.config/ranger
ADD ./to_copy/.vim/ /home/$USERNAME/.vim
ADD ./to_copy/.gitconfig/ /home/$USERNAME/.gitconfig
COPY ./to_copy/.vimrc /home/$USERNAME

RUN echo 'vim -u NONE -c "helptags ~/.vim/pack/vendor/start/nerdtree/doc" -c q'
RUN echo 'alias lolcat="/usr/games/lolcat"' >> ~/.bashrc
RUN echo 'alias fortune="/usr/games/fortune"' >> ~/.bashrc
RUN echo 'alias cowsay="/usr/games/cowsay"' >> ~/.bashrc
RUN echo 'alias histc="history -c"' >> ~/.bashrc
# some xterm-256 colors
#fg=046 # light green
#fg=127 # purple
#fg=202 # orange
#fg=208 # yellow/orange
#fg=063 # acceptable blue
#bg=000 # black
RUN echo 'LS_COLORS="di=1;35"' >> ~/.bashrc
RUN echo 'PS1="${debian_chroot:+($debian_chroot)}\[\033[38;5;34m\]\u@\h\[\033[00m\]:\[\033[38;5;208m\]\W \[\033[38;5;178m\]\$ \[\033[00m\]"' >> ~/.bashrc
