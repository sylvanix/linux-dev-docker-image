#!/bin/bash

username="syl"
imgname="syldev"
tag="haskell"
containername="dev-hk"

# run the image and drop into the container's shell
docker run \
    -it \
    --rm \
    --name ${containername} \
    --net=host \
    -h devnix \
    -e DISPLAY \
    -v ${HOME}/.Xauthority:/home/${username}/.Xauthority \
    -v ${HOME}/1210SRD:/1210SRD \
    ${imgname}:${tag} \
    env TERM=xterm-256color \
    /bin/bash
