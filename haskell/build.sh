#!/bin/bash

username="syl"
imgname="syldev"
tag="haskell"
containername="dev-hk"

# build from the Dockerfile
docker build . --tag ${imgname}:${tag}

