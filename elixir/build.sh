#!/bin/bash

username="syl"
imgname="syldev"
tag="elixir"
containername="dev-elixir"

# build from the Dockerfile
docker build . --tag ${imgname}:${tag}

