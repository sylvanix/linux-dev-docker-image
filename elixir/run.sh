#!/bin/bash

username="syl"
imgname="syldev"
tag="elixir"
containername="dev-elixir"

# run the image and drop into the container's shell
docker run \
    -it \
    --rm \
    --name ${containername} \
    --net=host \
    -h elixnix \
    -e DISPLAY \
    -v ${HOME}/.Xauthority:/home/${username}/.Xauthority \
    -v ${HOME}/1210SRD:/1210SRD \
    ${imgname}:${tag} \
    env TERM=xterm-256color \
    /bin/bash
