#!/bin/bash

username="syl"
imgname="pytorch"
tag="0.0.0"
containername="syl-pytorch"

#setfacl -m user:1000:r /home/${username}/.Xauthority

# build from the Dockerfile
docker build . --tag ${imgname}:${tag}

# run the image and drop into the container's shell
docker run \
    -it \
    --rm \
    --name ${containername} \
    --net=host \
    -h devnix \
    -e DISPLAY \
    -v ${HOME}/.Xauthority:/home/${username}/.Xauthority \
    -v ${HOME}/1210SRD:/1210SRD \
    ${imgname}:${tag} \
    env TERM=xterm-256color \
    /bin/bash
