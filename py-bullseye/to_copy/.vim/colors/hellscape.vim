" Vim color file
" adapted from Camilla Löwy's phosphor.vim
" color selection resource: http://www.color-hex.com/color/b3000a

set bg=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "hellscape"

" GUI useful colors
" light red    : ff000f
" red          : 
" pinkish      : c59690
" dark red     : 470004
" dim yellow   : e2ca00
" almost white : e0e0e0
" rusty red    : d54d00

" Basics
hi Normal       guifg=#ff000f  guibg=#000000  ctermfg=red
hi ErrorMsg     guifg=#ffffff  guibg=#ff0000  ctermfg=white      ctermbg=red
hi Visual       guifg=#001000  guibg=#ff7a00  ctermfg=lightgrey  ctermbg=darkblue
hi Todo         guifg=#001000  guibg=#00ff00  ctermfg=black      ctermbg=lightgreen
hi NonText      guifg=#ff000f                 ctermfg=lightred
hi Search       guifg=#001000  guibg=#ff0000                     ctermbg=yellow
hi Question     guifg=#80ff80                 ctermfg=lightred
hi MatchParen   guifg=#e2ca00  guibg=bg       ctermbg=yellow                        gui=bold
hi LineNr       guifg=#b11111  guibg=#330003  ctermfg=darkgray
hi Directory    guifg=#40d040                 ctermfg=lightred
hi Folded       guifg=#001000  guibg=#006000  ctermfg=black      ctermbg=darkgreen  gui=bold
hi ColorColumn  guibg=#470004                                    ctermbg=darkgray
hi Error        guibg=#ff8000                                    ctermbg=brown
hi CursorLine   guifg=#001000  guibg=#008000  ctermfg=black      ctermbg=green

" Splitter
hi StatusLine   guifg=#000000  guibg=#800007  ctermfg=black      ctermbg=red        gui=none  term=none      cterm=none
hi StatusLineNC guifg=#000000  guibg=#006000  ctermfg=black      ctermbg=darkgreen  gui=none  term=none      cterm=none
hi VertSplit    guifg=#000000  guibg=#006000  ctermfg=black      ctermbg=darkgreen  gui=none  term=none      cterm=none

" Popup menu
hi Pmenu        guifg=#80ff80  guibg=#002000  ctermfg=lightgreen ctermbg=darkgreen
hi PmenuSel     guifg=#001000  guibg=#80ff80  ctermfg=darkgreen  ctermbg=lightgreen
hi PmenuSbar    guibg=#104010
hi PmenuThumb   guibg=#40a040

" Code colors
hi Comment      guifg=#b11111  guibg=#330003  ctermfg=gray
hi pythonComment      guifg=#b11111  guibg=#330003  ctermfg=darkgray
hi Constant     guifg=#ff7a00                 ctermfg=lightred
hi Special      guifg=#ff000f                 ctermfg=lightred
hi Identifier   guifg=#b11111                 ctermfg=red
hi Statement    guifg=#ff0000                 ctermfg=red                           gui=none
hi PreProc      guifg=#ff000f                 ctermfg=red
hi Type         guifg=#b11111                 ctermfg=lightred                    gui=none
hi Underlined                                                                                 term=underline cterm=underline
hi Ignore       guifg=bg                      ctermfg=black
hi Operator     guifg=#b11111                 ctermfg=gray

