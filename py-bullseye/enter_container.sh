#!/bin/bash

username="syl"
containername="syl-dev"

# exec into the container's shell
docker exec \
    -it \
    --user ${username} \
    -e DISPLAY \
    ${containername} \
    env TERM=xterm-256color \
    /bin/bash
