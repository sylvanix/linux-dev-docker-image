#!/bin/bash

imgname="go-deb"
tag="dev"

# build from the Dockerfile
docker build . --tag ${imgname}:${tag}
